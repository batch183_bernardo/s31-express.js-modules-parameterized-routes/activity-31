const mongoose = require("mongoose");


// Creating Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

module.exports = mongoose.model("Task", taskSchema);